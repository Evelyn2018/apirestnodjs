### Para ejecutar la API REST por favor haga lo siguiente: ###

1. Clone el repositorio en una carpeta vacía dentro de su disco local.
2. Abra el editor de código que prefiera y agregue la carpeta del proyecto.
3. Asegurese de tener instalado Node Js, y el motor de base de datos PostgreSQL.
4. Descargue y ejecute el script de la base de datos. Ajuste la información de la cadena de conexión a la base de datos en el archivo database.js en la carpeta db.
5. En la consola de comandos ubiquese en la carpeta "prueba1", ejecute el comando "npm run dev" para subir la aplicación.
6. Al ejecutar la aplicación le debe aparecer un enlace de tipo http://localhost:4000/personas copielo y pongalo en el navegador o en una herramienta como insomnia para validar los endpoints.