import {Router} from 'express';

const router = Router();




import { crearPersona, listarPersona, obtenerPersona, actualizarPersona } from "../controllers/persona.controller"

router.get('/',listarPersona);

router.get('/:id',obtenerPersona);

router.post('/',crearPersona);

router.put('/:id',actualizarPersona);


export default router;