import Sequelize from 'sequelize';


export var cadena = new Sequelize( 'test', 
                'postgres', 
                '123456', 
                {   host: 'localhost', 
                    dialect: 'postgres', 
                    pool: { max:5, min:0, require: 30000, idle: 10000 } ,
                    logging: false
                }
             )