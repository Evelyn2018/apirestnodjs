import express, {json} from 'express';
import cors from 'cors';
import personaRoutes from './routes/personas'

const app = express();

app.use(cors());
app.use(json());

app.use('/personas',personaRoutes);

export default app;