import {  definirPersona } from "../models/Persona"


export async function listarPersona (req, res){
        const dbPersona = await definirPersona();
        const lista = await dbPersona.findAll( {include: ['HijoMadre', 'HijoPadre'] }); 
        res.json(lista );
     }

export async function obtenerPersona (req, res){
         const { id} = req.params;
        const dbPersona = await definirPersona();
        const p = await dbPersona.findOne({
                                where: { id: id} 
                                , include: ['HijoMadre', 'HijoPadre']
                             });
        if(p){
            res.json( p );
        }
        else {
            let n =  {
                id:0,
                nombre:'',
                documento:0  ,
                fnacimiento:'' ,
                idpadre: 0,
                idmadre:  0,
            }
            res.json(n );
        }
        
     }

export async function crearPersona (req, res){
    console.log(req.body);
    const dbPersona = await definirPersona();
    try {
        const { nombre, documento, fnacimiento, idpadre, idmadre } = req.body;
        const p = await dbPersona.findOne({
                    where: { documento: documento} 
                     });
        if(p){
            res.status(500).json("Ya existe una persona registrada con este documento. "
            );
        }

        let nuevo =  await dbPersona.create({
            nombre: nombre,
            documento: documento,
            fnacimiento: fnacimiento,
            idpadre: idpadre,
            idmadre: idmadre
        })
        if(nuevo){
            res.json({
                mensaje: "Persona Creada correctamente",
                datos: nuevo
            });
        }
    } catch (error) {
        res.status(500).json({
            mensaje: "Error al crear Persona "+error,
            datos: {}
        });
    }
 }

export async function actualizarPersona (req, res){
     console.log(req.body);
     const { id} = req.params;
     const { nombre, documento, fnacimiento,idpadre, idmadre } = req.body;
     const dbPersona = await definirPersona();
     const p = await dbPersona.findOne({ where: { id: id}});
     await p.update({
            nombre : nombre,
            documento: documento,
            fnacimiento: fnacimiento,
            idpadre: idpadre,
            idmadre: idmadre
        })
         
     res.json({
        mensaje: "Prueba actualizacion",
        datos: p
    });
     }

   

   
